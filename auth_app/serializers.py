from django.contrib.auth import get_user_model

from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ValidationError

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    password2 = serializers.CharField(max_length=155, write_only=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'password2')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data['password']
        password2 = validated_data['password2']
        if password != password2:
            raise ValidationError('Password1 != Password2')

        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        user.set_password(password)
        user.save()
        Token.objects.create(user=user)
        return user
