from django.urls import path

from auth_app.views import LoginView, UserCreate

app_name = 'auth_app'

urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("register/", UserCreate.as_view(), name="user_create"),
]
