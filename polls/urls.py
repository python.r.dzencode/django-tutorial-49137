from django.urls import path

from rest_framework.routers import DefaultRouter

from polls.views import PollViewSet, ChoiceListView, CreateVote, CreateChoiceView

app_name = 'polls'
router = DefaultRouter()
router.register('', PollViewSet, basename='polls')

urlpatterns = [
    path("<int:pk>/choices/", ChoiceListView.as_view(), name="choice_list"),
    path("<int:pk>/choices/create/", CreateChoiceView.as_view(), name="choice_list"),
    path("<int:pk>/choices/<int:choice_pk>/vote/", CreateVote.as_view(), name="create_vote"),
]

urlpatterns += router.urls
