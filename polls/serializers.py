from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from polls.models import Poll, Choice, Vote


class VoteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vote
        fields = '__all__'


class ChoiceSerializer(serializers.ModelSerializer):

    votes = VoteSerializer(many=True, required=False)

    class Meta:
        model = Choice
        fields = '__all__'

    def create(self, validated_data):
        poll = validated_data.get('poll')
        request = self.context.get('request')
        if request.user != poll.created_by:
            raise ValidationError({'detail': 'You don`t have access for this action'})

        return super(ChoiceSerializer, self).create(validated_data)


class PollSerializer(serializers.ModelSerializer):

    choices = ChoiceSerializer(many=True, read_only=True, required=False)

    class Meta:
        model = Poll
        fields = '__all__'
