from django.contrib import admin

from polls.models import Poll, Choice, Vote


@admin.register(Poll)
class PollAdmin(admin.ModelAdmin):
    list_display = ('question', 'created_by', 'pud_date')
    list_display_links = ('question', )
    readonly_fields = ('pud_date', )


@admin.register(Choice)
class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('poll', 'choice_text')
    list_display_links = ('poll', )


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = ('choice', 'poll', 'voted_by')
    list_display_links = ('choice', 'poll')
